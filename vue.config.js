/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-15 10:09:35
 * @LastEditTime: 2021-11-28 21:41:50
 * @Description: file context
 */
const path = require('path')
const resolvePath = (dir) => path.join(__dirname, dir)

module.exports = {
  //   change import path of require modules
  chainWebpack: (config) => {
    config.resolve.alias.set('@', resolvePath('src'))

    // svg setting
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule.use('svg-sprite-loader').loader('svg-sprite-loader').options({
      symbolId: 'icon-[name]'
    })
  }
}
