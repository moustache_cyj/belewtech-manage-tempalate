From node:14 as build
WORKDIR /app
COPY package*.json ./
COPY babel.config.js ./
COPY vue.config.js ./
COPY .eslintrc.js ./

RUN npm install
COPY src  src/
COPY public public/
RUN npm run build

FROM nginx:alpine
COPY --from=build /app/dist/  /usr/share/nginx/html
EXPOSE 80
CMD ["nginx","-g","daemon off;"]