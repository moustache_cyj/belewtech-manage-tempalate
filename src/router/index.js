/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 13:12:16
 * @LastEditTime: 2022-02-10 10:31:40
 * @Description: file context
 */
import { createRouter, createWebHashHistory } from 'vue-router'
import store from '@/store/index'
// import Home from '../views/Home.vue'
import Layout from '@/components/layout/index.vue'
import Demo from '@/views/Demo.vue'
const requireComponent = require.context('../views', false, /\.vue$/)
const routes = [
  {
    name: 'manage',
    path: '/manage',
    component: Layout,
    meta: { title: '系统管理' },
    children: []
  },
  {
    name: 'demo',
    path: '/demo',
    component: Demo
  },
  // {
  //   path: '/system',
  //   name: 'system',
  //   component: Layout,
  //   redirects: '/system/user',
  //   meta: { title: '系统管理' },
  //   children: [
  //     {
  //       path: '//user',
  //       name: 'user',
  //       meta: { title: '用户管理' },
  //       component: () =>
  //         import(/* webpackChunkName: "about" */ '../views/UserManage.vue')
  //     },
  //     {
  //       path: '/system/menu',
  //       name: 'menu',
  //       meta: { title: '菜单管理' },
  //       component: () =>
  //         import(/* webpackChunkName: "about" */ '../views/MenuManage.vue')
  //     },
  //     {
  //       path: '/system/role',
  //       name: 'role',
  //       meta: { title: '角色管理' },
  //       component: () =>
  //         import(/* webpackChunkName: "about" */ '../views/RoleManage.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/roleManage',
  //   name: 'roleManage',
  //   component: Layout,
  //   redirects: '/roleManage/roleList',
  //   children: [
  //     {
  //       path: '/roleManage',
  //       name: 'roleManage',
  //       redirects: '/roleManage/roleList',
  //       component: () =>
  //         import(/* webpackChunkName: "about" */ '../views/RoleManage.vue')
  //     }
  //   ]
  // },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Login.vue')
  }
]
const router = createRouter({
  history: createWebHashHistory(),
  routes
})

function setRouterList () {
  const menuList = JSON.parse(JSON.stringify(store.state.user.menuList))
  const routerList = menuList.filter(
    (m) => m.menuType === 1 && m.parentId.length > 0
  )
  routerList.map((r) => {
    // sure component exist
    if (r.component) {
      const url = `./${r.component}.vue`
      r.name = r.menuName
      r.meta = {}
      r.meta.title = r.menuName
      // import 无法导入该用required 大坑
      r.component = requireComponent(url).default
      router.addRoute('manage', r)
    }
  })
}

router.beforeEach(async (to, from, next) => {
  if (to.name) {
    if (router.hasRoute(to.name)) {
      document.title = to.name
      next()
    } else {
      next('/404')
    }
  } else {
    setRouterList()
    next(to.path)
    // const curRoute = router.getRoutes().filter((item) => item.path == to.path)
    // if (curRoute?.length) {
    //   document.title = curRoute[0].meta.title
    //   next({ ...to, replace: true })
    // } else {
    //   next('/404')
    // }
  }
})

export default router
