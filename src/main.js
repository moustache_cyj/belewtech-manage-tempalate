/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 13:12:16
 * @LastEditTime: 2022-02-10 14:06:32
 * @Description: file context
 */
import { createApp } from 'vue'
import App from './App.vue'
import '@/registerServiceWorker'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import api from './api/index'
import SVGICON from './icon/index.vue'
import QueryForm from '@/components/queryForm/queryForm.vue'
// element-ui about css
import 'element-plus/dist/index.css'

const app = createApp(App)
app.directive('has', {
  beforeMount: function (el, binding) {
    const menuCode = binding.value
    const action = store.state.user.action
    const hasPermission = action.includes(menuCode)
    if (!hasPermission && store.state.user.userInfo.role === 1) {
      el.style.display = 'none'
      setTimeout(() => {
        el.parentNode.removeChild(el)
      })
    }
  }
})
app.config.globalProperties.$api = api
app.config.devtools = true
app
  .component('QueryForm', QueryForm)
  .component('bsvg', SVGICON)
  .use(ElementPlus)
  .use(store)
  .use(router)
  .mount('#app')
