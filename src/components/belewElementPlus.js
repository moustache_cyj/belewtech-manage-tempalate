/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-20 12:16:40
 * @LastEditTime: 2021-11-20 13:08:10
 * @Description: 对于elment一些组件的封装
 */
import { ElMessageBox } from 'element-plus'
const ComfirmOpt = {
  confirmButtonText: '确认',
  cancelButtonText: '取消',
  type: 'warning',
  center: true
}
const belewELementPlus = () => {
  const openComfirm = (ctx, title, type = 'warning', options = ComfirmOpt) => {
    options.type = type
    return new Promise((resolve, reject) => {
      ElMessageBox.confirm(ctx, title, options)
        .then(() => resolve({ status: true }))
        .catch(e => {
          reject(new Error(false))
        })
    })
  }
  return {
    openComfirm
  }
}
export default belewELementPlus
