/*
 * @Author: Moustach_壮壮
 * @Date: 2022-02-01 16:47:20
 * @LastEditTime: 2022-02-11 17:54:21
 * @Description: file context
 */
export default {
  state: {
    tabs: [],
    visibleTab: ''
  },
  mutations: {
    SET_TABS: (state, tab) => {
      const l = state.tabs
      const index = l.findIndex((v) => v._id === tab._id)
      if (!(index > -1)) l.push(tab)
      state.visibleTab = tab.path
      state.tabs = l
    },
    DELET_TABS: (state, path) => {
      const l = state.tabs
      const index = l.findIndex((v) => v.path === path)
      if (index > -1 && l.length > 1) state.tabs.splice(index, 1)
      state.visibleTab = state.tabs[index - 1].path
    },
    ClEAR_TABS: (state, value) => {
      state.tabs = []
    }
  }
}
