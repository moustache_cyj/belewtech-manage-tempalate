/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-16 14:18:38
 * @LastEditTime: 2021-11-27 22:02:58
 * @Description: file context
 */
export default {
  state: {
    userInfo: '',
    token: '',
    action: '',
    tree: ''
  },
  mutations: {
    SET_USERINFO (state, userInfo) {
      state.userInfo = userInfo
    },
    SET_TOKEN (state, token) {
      state.token = token
    },
    SET_ACTION (state, action) {
      state.action = action
    },
    SET_MENULIST (state, value) {
      state.tree = value
    },
    SET_MENU (state, value) {
      state.menuList = value
    }
  }
}
