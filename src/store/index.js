/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-15 10:09:35
 * @LastEditTime: 2022-02-11 16:49:24
 * @Description: file context
 */
import { createStore } from 'vuex'
import User from './modules/user'
import Tabs from './modules/tabs'
import persistedstate from 'vuex-persistedstate'
export default createStore({
  plugins: [persistedstate()],
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user: User,
    tabs: Tabs
  }
})
