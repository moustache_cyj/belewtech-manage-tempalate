/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 21:15:34
 * @LastEditTime: 2021-11-15 10:17:00
 * @Description: localStorage api
 */
import CONFIG from '@/config'
export default {
  getStorage () {
    return JSON.parse(window.localStorage.getItem(CONFIG.namespace) || '{}')
  },

  setItem (key, value) {
    const storage = this.getStorage()
    storage[key] = value
    window.localStorage.setItem(CONFIG.namespace, JSON.stringify(storage))
  },

  getItem (key) {
    return this.getStorage()[key]
  },

  deleteStorage (key) {
    const storage = this.getStorage()
    delete storage[key]
    window.localStorage.setItem(CONFIG.namespace, JSON.stringify(storage))
  },

  clearAll () {
    window.localStorage.clear()
  }
}
