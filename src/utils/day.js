/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-19 15:58:46
 * @LastEditTime: 2021-11-19 16:27:24
 * @Description: file context
 */
import dayjs from 'dayjs'
export const formatterTime = (value) => {
  return dayjs(value).format('YYYY-MM-DD HH:mm:ss')
}
export default {
  formatterTime
}
