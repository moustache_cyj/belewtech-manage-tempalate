/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 19:48:44
 * @LastEditTime: 2022-02-10 15:28:15
 * @Description: requist instance
 */
import axios from 'axios'
import CONFIG from '@/config/index.js'
import { ElMessage } from 'element-plus'
import store from '@/store/index'
const service = axios.create({
  baseURL: CONFIG.BASE_URL,
  timeout: 8000
})

service.interceptors.request.use((req) => {
  // TODE token
  const headers = req.headers
  if (!headers.authorization) headers.authorization = store.state.user.token
  return req
})

service.interceptors.response.use(
  (res) => {
    return res.data
  },
  (e) => {
    ElMessage.error('网络请求错误')
  }
)

/**
 * @description: fixed requset method
 * @param {*} options
 * @return {*} axios promise
 */
const request = (options) => {
  // 去除对象假值
  if (options.data) {
    for (const key in options.data) {
      if (options.data[key] === '') {
        delete options.data[key]
      }
    }
  }
  if (options.method.toLocaleLowerCase() === 'get') {
    options.params = options.data
  }
  return service(options)
}

export default request
