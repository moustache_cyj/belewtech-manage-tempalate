/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 13:23:11
 * @LastEditTime: 2022-02-21 15:40:02
 * @Description: config file
 */
const env = process.env.NODE_ENV || 'product'
const config = {
  development: {
    // BASE_URL: 'http://mock.belew.tech/mock/478/'
    // BASE_URL: 'http://192.168.50.126:3000'
    BASE_URL: 'http://localhost:3000'
  },
  product: {
    BASE_URL: 'http://localhost:3000'
    // BASE_URL: '/'
  }
}
export default {
  env,
  // localStorage namespace
  namespace: 'manage',
  ...config[env]
}
