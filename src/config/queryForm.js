/*
 * @Author: Moustach_壮壮
 * @Date: 2022-02-10 14:14:52
 * @LastEditTime: 2022-02-11 14:30:37
 * @Description: file context
 */
export const menuManageQuery = [
  {
    valueName: 'menuName',
    options: {
      span: 4,
      placeholder: '请输入菜单名字',
      type: 'input'
    }
  },
  {
    valueName: 'menuState',
    options: {
      span: 4,
      placeholde: '请输入菜单名称',
      type: 'select',
      optionList: [
        {
          label: '启用',
          value: 1
        },
        {
          label: '禁用',
          value: 2
        }
      ]
    }
  }
]
export const roleManageQuery = [
  {
    valueName: '_id',
    options: {
      span: 4,
      placeholder: '请输入角色id',
      type: 'input'
    }
  },
  {
    valueName: 'roleName',
    options: {
      span: 4,
      placeholder: '请输入角色名字',
      type: 'input'
    }
  }
]
export const userManageQuery = [
  {
    valueName: 'userName',
    options: {
      span: 4,
      placeholder: '请输入用户名',
      type: 'input'
    }
  },
  {
    valueName: 'userId',
    options: {
      span: 4,
      placeholder: '请输入用户Id',
      type: 'input'
    }
  }
]
