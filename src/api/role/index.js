/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-20 16:49:25
 * @LastEditTime: 2021-11-25 15:26:05
 * @Description: file context
 */
import request from '@/utils/request'

export default {
  getRoleList: (data) =>
    request({ url: '/api/role/list', method: 'get', data }),
  addRole: (data) => request({ url: '/api/role/add', method: 'post', data }),
  editRole: (data) => request({ url: '/api/role/update', method: 'put', data }),
  deleteRole: (data) =>
    request({ url: '/api/role/delete', method: 'delete', data }),
  getRoleDetail: (_id) => {
    const data = { _id }

    return request({ url: '/api/role/detail', method: 'get', data })
  },
  addRolePermisson: (data) =>
    request({ url: '/api/role/permission/update', method: 'put', data })
}
