/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 13:23:03
 * @LastEditTime: 2021-11-26 10:59:06
 * @Description: user api
 */
import request from '@/utils/request'

export default {
  login: (data) => request({ url: '/api/user/login', method: 'post', data }),
  getUserList: (data) =>
    request({ url: '/api/user/list', method: 'get', data }),
  getAllUser: () => request({ url: '/api/user/all/list', method: 'get' }),
  addUser: (data) => request({ url: '/api/user/add', method: 'post', data }),
  editUser: (data) => request({ url: '/api/user/update', method: 'put', data }),
  deleteUser: (data) =>
    request({ url: '/api/user/delete', method: 'delete', data }),
  getPermissionList: () =>
    request({ url: '/api/user/getPermissionList', method: 'get' })
}
