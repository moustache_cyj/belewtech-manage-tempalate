/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 20:25:58
 * @LastEditTime: 2021-11-24 14:17:52
 * @Description: about all api
 */
import userApi from '@/api/user'
import roleApi from '@/api/role'
import menuApi from '@/api/menu'
export default {
  ...userApi,
  ...roleApi,
  ...menuApi
}
