/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-14 13:23:03
 * @LastEditTime: 2022-02-10 15:28:49
 * @Description: user api
 */
import request from '@/utils/request'

export default {
  getMenuList: (data) => {
    return request({ url: '/api/menu/list', method: 'get', data })
  },
  addMenu: (data) => {
    return request({ url: '/api/menu/add', method: 'post', data })
  },
  editMenu: (data) => {
    return request({ url: '/api/menu/edit', method: 'put', data })
  },
  deleteMenu: (id) => {
    const data = { _id: id }
    return request({ url: '/api/menu/delete', method: 'delete', data })
  }
}
